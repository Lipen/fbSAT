object Versions {
    const val kotlin = "1.3.21"
    const val ktlint = "7.1.0"
    const val clikt = "1.6.0"
    const val junit = "5.4.0"
    const val kluent = "1.48"
}

object Libs {
    const val clikt = "com.github.ajalt:clikt:${Versions.clikt}"
    const val junit_jupiter_api = "org.junit.jupiter:junit-jupiter-api:${Versions.junit}"
    const val junit_jupiter_engine = "org.junit.jupiter:junit-jupiter-engine:${Versions.junit}"
    const val junit_jupiter_params = "org.junit.jupiter:junit-jupiter-params:${Versions.junit}"
    const val kluent = "org.amshove.kluent:kluent:${Versions.kluent}"
}
